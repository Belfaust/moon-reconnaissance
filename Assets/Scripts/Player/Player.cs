﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UIElements;

public class Player : MonoBehaviour
{
    public bool dead = false;
    private Rigidbody _rigidbody;
    private List<GameObject> _bulletPool = new List<GameObject>();
    [SerializeField]internal int _lives;
    [SerializeField] private List<Vector3> _barrelPlacement = new List<Vector3>();
    [SerializeField]private GameObject _bullet;
    [SerializeField] private float _speed;
    [SerializeField] private float _jumpVelocity;
    [SerializeField] private LayerMask _jumpMask;

    
    private void Start() {
        _rigidbody = GetComponent<Rigidbody>();
        PoolBullets();
    }
    public void PoolBullets()
    {
        for (int i = 0; i < 20; i++)
        {
            GameObject pooledBullet = Instantiate(_bullet, transform.position, Quaternion.identity,GameController.instance.transform);
            pooledBullet.SetActive(false);
            _bulletPool.Add(pooledBullet);
        }
    }
    private void Update()
    {
        if (dead == false)
        {
            Stabilize();
            Jumping();
            Moving();
            Shooting();
        }
    }

    private void Shooting()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            foreach (var bullet in _bulletPool)
            {
                if (!bullet.activeInHierarchy)
                {
                    bullet.transform.position = transform.position + _barrelPlacement[0];
                    bullet.transform.rotation = Quaternion.Euler(0,0,-90);
                    bullet.SetActive(true);
                    break;
                }
            }

            foreach (var bullet in _bulletPool)
            {
                if (!bullet.activeInHierarchy)
                {
                    bullet.transform.position = transform.position + _barrelPlacement[1];
                    bullet.SetActive(true);
                    bullet.transform.rotation = Quaternion.Euler(0,0,0);
                    break;
                }
            }
        }
    }

    private void Stabilize()
    {
        if (transform.eulerAngles.z > 10&&transform.eulerAngles.z < 180)
        {
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, 10);
        }
        if (transform.eulerAngles.z >180 &&transform.eulerAngles.z < 350)
        {
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, 350);
        }
    }
    private void Jumping()
    {
        if(Input.GetKeyDown(KeyCode.Space)&&GroundCheck())
        {
            _rigidbody.velocity = new Vector3(_rigidbody.velocity.x,0,_rigidbody.velocity.z);
            _rigidbody.velocity += Vector3.up * _jumpVelocity;
        }
    }
    private void Moving()
    {
        if (Input.GetAxisRaw("Horizontal")>0)
        {
            _rigidbody.velocity += new Vector3(_speed*Time.deltaTime,0,0);
        }
        else if(Input.GetAxisRaw("Horizontal")<0)
        {
            _rigidbody.velocity -= new Vector3(_speed*Time.deltaTime,0,0);
        }
    }
    bool GroundCheck()
    {
        if(Physics.Raycast(transform.position,-Vector3.up,1,_jumpMask))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private void Die(int livesLost)
    {
        if(_lives -livesLost >=0)
        {
         _lives -= livesLost;
        }
        dead = true;
        StartCoroutine(GameController.instance.GameoverCheck(_lives));
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy")&&dead== false)
        {
            Die(1);
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Bounds"))
        {
            if (Input.GetAxisRaw("Horizontal") > 0)
            {
                _rigidbody.velocity -= new Vector3(_rigidbody.velocity.x * Time.deltaTime * 3, 0, 0);
                GameController.instance.FastenTerrain(_rigidbody.velocity.x);
            }
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        var collisionObject = other.gameObject;
        if (collisionObject.TryGetComponent(out BoulderAI boulder)&&!dead)
        {
            GetComponent<Rigidbody>().velocity = new Vector3(0,GetComponent<Rigidbody>().velocity.y,0);
            Destroy(other.gameObject);
            Die(1);
        }
    }
    
    private void OnDrawGizmosSelected()
    {
       Gizmos.DrawSphere(transform.position +_barrelPlacement[0],.1f);
       Gizmos.DrawSphere(transform.position +_barrelPlacement[1],.1f); 
    }
}
