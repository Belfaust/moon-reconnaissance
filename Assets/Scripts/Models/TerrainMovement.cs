﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class TerrainMovement : MonoBehaviour
{
    public TerrainController terrainController;
    public bool isaPit;
    public int segmentMeshLength;
    public float Speed = 7;
    [SerializeField] private bool pooled = true;
    void Update()
    {
    transform.Translate(new Vector3(-Speed*Time.deltaTime,0,0));       
    }

    private void OnBecameInvisible()
    {
        if (transform.position.x < -5&&pooled == true)
        {
            var prefabList = terrainController.terrainPool;
            var currentIndex = prefabList.IndexOf(gameObject);
            if (currentIndex-1 < 0)
            {
                prefabList[currentIndex].transform.position = new Vector3(prefabList[prefabList.Count-1].transform.position.x +segmentMeshLength-1,prefabList[prefabList.Count-1].transform.position.y,0);
            }
            else
            {
                prefabList[currentIndex].transform.position = new Vector3(prefabList[currentIndex-1].transform.position.x +segmentMeshLength-1,prefabList[currentIndex-1].transform.position.y,0);
            }
            GameController.instance.ResetMesh(prefabList[currentIndex]);
        }
        else if (pooled == false)
        {
            Destroy(gameObject);
        }
    }
}
