﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;


public class GameController : MonoBehaviour
{
    public static GameController instance;
    public bool setPit = false;
    public bool CountTime
    {
        get => _countingTime;
        set
        {
            if(value&&value != _countingTime)
            {
                _countingTime = value;
                StartCoroutine(CountSectorTime());
            }
            else
            {
                StopCoroutine(CountSectorTime());
            }
            _countingTime = value;
        }
    }
    private bool _countingTime;
    private DateTime checkpointTime;
    private int _maxLevelProgress = 520;
    [SerializeField]private UIController _uiController;
    [SerializeField]private AIController _aiController;
    [SerializeField]private Player _player;
    [SerializeField] private GameObject _standardMapMesh,_chunksList;
    [SerializeField]private int _points,_levelProgress = 10;
    private TerrainController _terrainController;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }

        if (PlayerPrefs.GetInt(Convert.ToChar((int) (_levelProgress / 20) + 64).ToString()) > 5)
        {
            ResetRecords();
        }
    }
    public void StartGame()
    {
        ResetGame();
        _terrainController = new TerrainController();
        var startingMap = Instantiate(_standardMapMesh, new Vector3(-10, -10, 0), Quaternion.identity);
         startingMap.GetComponent<MeshFilter>().sharedMesh = _terrainController.CreateStartingLevel(1,1,new Vector3(20,10,0)).CreateMesh();
         startingMap.AddComponent<MeshCollider>();
         startingMap.GetComponent<TerrainMovement>().Speed = 7;
        _terrainController.CreateStartingMeshPool(_standardMapMesh.GetComponent<MeshRenderer>(),_chunksList);
        
        ResetPlayer();
    }
    public void ResetMesh(GameObject gameObject)
    {
        _terrainController.ApplyNewMesh(gameObject);
        Destroy(gameObject.GetComponent<MeshCollider>());
        gameObject.AddComponent(typeof(MeshCollider));
    }
    private void ReAdjustLevel()
    {
        for (int i = 0; i < _terrainController.terrainPool.Count; i++)
        {
            ResetMesh(_terrainController.terrainPool[i]);
            _terrainController.terrainPool[i].transform.position = new Vector3((i*5)-9,-10,0);
        }
    }
    public void ProgressLevel(int value)
    {
        _levelProgress += value;
        _uiController.UpdateProgressBar(_levelProgress);
        if (_levelProgress >= _maxLevelProgress)
        {
            StartCoroutine(HighScoreInsert());
            StopGame();
            _uiController.DisplayHighScoreInput();
        }
        else if (_levelProgress > 25&& _levelProgress <_maxLevelProgress-25)
        {
            if (_levelProgress % (_maxLevelProgress / 5) == 0)
            {
                DisplayCheckpoint();
            }
            else if(CountTime == true)
            {
                if (_levelProgress % 15 == 0)
                {
                    setPit = true;
                    _aiController.ExecuteRandomWave();
                }

                if (_levelProgress % 19 == 0 )//&& _levelProgress / _maxLevelProgress > .25f)
                {
                    StartCoroutine(_aiController.BoulderWave(3));
                }    
            }
        }
    }
    private void ResetGame()
    {
        ClearMap();
        _points = 0;
        _uiController.UpdateScore(0);
        _levelProgress = 10;
        _uiController.SetProgressBar(0,_maxLevelProgress,_levelProgress);
        CountTime = true;
        checkpointTime = DateTime.Now;
    }

    private void ResetPlayer()
    {
        _player.gameObject.transform.position =  new Vector3(-4,3);
        _player.gameObject.SetActive(true);
        _player.dead = false;
        _player._lives = 3;
        _uiController.UpdateLives(3);
    }
    private void DisplayCheckpoint()
    {
        StopGame();
        TimeSpan currentCheckpointTime = DateTime.Now - checkpointTime;
        _uiController.MainSector(_levelProgress,(int)currentCheckpointTime.TotalSeconds);
        
        checkpointTime = DateTime.Now;
        _uiController.UpdateTime(0);
    }
    public void AddPoints(int pointsToAdd)
    {
        AddingLives(pointsToAdd);
        _points += pointsToAdd;
        _uiController.UpdateScore(_points);
    }
    public void FastenTerrain(float speed)
    {
        foreach (var chunk in _terrainController.terrainPool)
        {
            chunk.transform.Translate( new Vector3(-speed*Time.deltaTime,0,0)    );
        }
    }
    public void StopGame()
    {
        CountTime = false;
        _aiController.CleanEnemies();
        _aiController.StopExecuting();
        _terrainController.SetAllSpeed(0);
        _player.dead = true;
        _player.gameObject.GetComponent<Rigidbody>().Sleep();
    }

    public void ResumeGame()
    {
        CountTime = true;
        _terrainController.SetAllSpeed(7);
        _player.transform.position = new Vector3(-4,0,0);
        _player.dead = false;
        _player.gameObject.GetComponent<Rigidbody>().WakeUp();
    }
    private void ResetRecords()
    {
        for (int i = _maxLevelProgress / 5; i <= _maxLevelProgress; i += (_maxLevelProgress / 5))
        {
            PlayerPrefs.SetInt(Convert.ToChar((int) (i / 20) + 64).ToString(), 100);
        }
    }
    private void ClearMap()
    {
        for (int i = 0; i < _chunksList.transform.childCount; i++)
        {
            Destroy(_chunksList.transform.GetChild(i).gameObject);
        }
    }

    private void AddingLives(int addedPoints)
    {
        switch (_points/5000)
        {
            case 0:
                LiveTresholdCheck(addedPoints, 5000);
                break;
            case 1:
                LiveTresholdCheck(addedPoints, 10000);
                break;
            case 3:
                LiveTresholdCheck(addedPoints, 20000);
                break;
            case 5:
                LiveTresholdCheck(addedPoints, 30000);
                break;
            case 9:
                LiveTresholdCheck(addedPoints, 50000);
                break;
        }
    }
    private void LiveTresholdCheck(int addedPoints,int treshold)
    {
        if (_points + addedPoints >= treshold
            &&_player._lives < 5 )
        {
            _player._lives += 1;
            _uiController.UpdateLives(_player._lives);
        }
    }
    public IEnumerator GameoverCheck(int lives)
    {
        while (true)
        {
            _uiController.UpdateLives(lives);
            _terrainController.SetAllSpeed(0); 
            yield return new WaitForSeconds(1.5f);
            if (lives > 0)
            {
                _aiController.CleanEnemies();
                if ((_levelProgress - (_levelProgress % 26)) % (_maxLevelProgress / 5)== 0)
                {
                    _levelProgress = _levelProgress - (_levelProgress % 26)+1;

                }
                else
                {
                    _levelProgress = _levelProgress - (_levelProgress % 26);
                }
                ReAdjustLevel();
                ResumeGame();
                _player.transform.position = new Vector3(-4,0,0);
            }
            else
            {
                CountTime = false;
                _aiController.CleanEnemies();
                StartCoroutine(HighScoreInsert());
            }
            break;
        }
    }
    IEnumerator HighScoreInsert()
    {
        while (true)
        {
            Camera.main.transform.DOMoveX(22.5f, 5);
            yield return new WaitUntil(() => Camera.main.transform.position.x > 20f);
            _player.gameObject.SetActive(false);
            Camera.main.transform.DOMoveY(20f, 5);
            yield return new WaitForSeconds(.5f);
            _uiController.DisplayHighScoreInput();
            break;
        }
    }
    IEnumerator CountSectorTime()
    {
        while (CountTime)
        {
            TimeSpan timeTillCheckpoint = DateTime.Now - checkpointTime;
            _uiController.UpdateTime((int)timeTillCheckpoint.TotalSeconds);
            yield return new WaitForSeconds(1);
        }
    }
    public int GetScore()
    {
        return _points;
    }
}


