﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoulderAI : AIBase
{
    private Rigidbody _rb;
    private bool _groundCheck = false;

    private void Start()
    {
        _rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        _rb.AddForce(new Vector3(-_speed*Time.deltaTime,0,0),ForceMode.Force);
        var myRay = Physics.Raycast(transform.position + new Vector3(-1, 0, 0), Vector3.down, 1);
        if (!myRay&&_groundCheck== true)
        {
            //_rb.AddForce(new Vector3(0,10,0),ForceMode.Impulse);
            _groundCheck = false;
        }

        if (myRay && !_groundCheck)
        {
            _groundCheck = true;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Bullet bullet)&&!other.CompareTag("Enemy"))
        {
            GameController.instance.AddPoints(_points);
            Destroy(gameObject);
            other.gameObject.SetActive(false);

        }
    }
}
