﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoundAI : AIBase
{
    public float reloadTime;
    private void Awake()
    {
        ShootPlayer();
    }
    private void Update()
    {
        Behave();
    }
    protected override void Behave()
    {
        base.Behave();   
    }
    protected override void OnDeath()
    {
        base.OnDeath();
    }
    protected override void ShootPlayer()
    {
        base.ShootPlayer();
        StartCoroutine(Reload());
    }

    IEnumerator Reload()
    {
        while(true)
        {
            yield return new WaitForSeconds(reloadTime);
            ShootBullet(Quaternion.Euler(0,0,25));
            ShootBullet(Quaternion.Euler(0,0,-25));
        }
    }

    public void ShootBullet(Quaternion angle)
    {
        GameObject Bullet = Instantiate(bullet, transform.position, Quaternion.Euler(0, 0, -180),transform.parent);
        Bullet.GetComponent<Rigidbody>().velocity = new Vector3(0,Random.Range(-.1f,.1f),0);
        Bullet.transform.rotation *=angle;
    }
}
