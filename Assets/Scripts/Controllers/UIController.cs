﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [SerializeField]private TextMeshProUGUI _sectorLetterDisplay,_lifeCount,_timeDisplay;
    [SerializeField] private SectorCutscene _cutscene;
    [SerializeField]private Slider _progressBar;
    [SerializeField]private GameObject MainUI,_highscoreInput,_highscoreTable;
    private ScoreCounter _scoreCounter;
    private void Start()
    {
        _scoreCounter = GetComponent<ScoreCounter>();
        //PlayerPrefs.DeleteAll();
     //   _highScoreDisplay.text = string.Format("{0}{1}{3}{4}",)
       // 
    }

    public void UpdateScore(int pointsToUpdate)
    {
        _scoreCounter.UpdatePoints(pointsToUpdate, _progressBar);
    }
    public void MainSector(int levelProgress,int time)
    {
        StartCoroutine(_cutscene.MainSectorStop(levelProgress, time, _progressBar));
    }
    public void SetProgressBar(int minValue,int maxValue,int currentValue)
    {
        _progressBar.minValue = minValue;
        _progressBar.maxValue = maxValue;
        _progressBar.value = currentValue;
    }
    public void UpdateProgressBar(int value)
    {
        _progressBar.value = value;
        if (_progressBar.value % 10 == 0&&_progressBar.value>5)
        {
            _sectorLetterDisplay.text = $"POINT {Convert.ToChar((value / 20) + 64).ToString()}";
        }
    }
    public void UpdateLives(int value)
    {
        _lifeCount.text = value.ToString();
    }

    public void BeginGame()
    {
        StartCoroutine(nameof(StartingCutscene));
    }

    public void UpdateTime(int time)
    {
        _timeDisplay.text = $"Time {time.ToString()}";
    }

    public void DisplayHighScoreInput()
    {
        MainUI.SetActive(false);
        _highscoreInput.SetActive(true);
    }

    public void MoveToHighScore()
    {
        StartCoroutine(HighScoreTransition());
    }

    public void MoveToMenu()
    {
        _highscoreTable.SetActive(false);
        Camera.main.transform.DOMoveX(1.5f, 3);
    }
    private IEnumerator StartingCutscene()
    {
        while(true)
        {
            Camera.main.transform.DOMoveY(3.5f, 5);
            yield return new WaitUntil(() => Camera.main.transform.position.y < 10f);
            GameController.instance.StartGame();
            yield return new WaitForSeconds(.5f);
            MainUI.SetActive(true);
            break;
        }
    }
    
    IEnumerator HighScoreTransition()
    {
        while (true)
        {
            Camera.main.transform.DOMoveX(24, 3);
            yield return new WaitUntil(() => Camera.main.transform.position.x > 22f);
            _highscoreTable.SetActive(true);
            break;
        }
    }
}
