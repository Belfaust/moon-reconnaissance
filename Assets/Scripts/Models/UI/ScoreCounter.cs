﻿using System;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ScoreCounter : MonoBehaviour
{
    [SerializeField]private TextMeshProUGUI _points,_highScoreDisplay;
    private const string Space = " ";

    private void Start()
    {
        SetHighScoreString();
    }

    public void UpdatePoints(int currentPoints,Slider _progressBar)
    {
        _points.text = $"Score: {currentPoints.ToString()}";
        if (currentPoints > PlayerPrefs.GetInt("HighScore", 0))
        {
            PlayerPrefs.SetInt("HighScore",currentPoints);
            
            PlayerPrefs.SetString("HighScore Sector",Convert.ToChar((int)(_progressBar.value / 20)+64).ToString());

            SetHighScoreString();
        }
    }

    private void SetHighScoreString()
    {
        _highScoreDisplay.text = $"HighScore :{PlayerPrefs.GetInt("HighScore")}{Space}{PlayerPrefs.GetString("HighScore Sector")}";
    }
}
