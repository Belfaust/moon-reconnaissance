﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class AIBase : MonoBehaviour
{
    public Vector3 leftSpawnCorner, rightSpawnCorner;
    public GameObject bullet;
    private int _hp;
    private Vector3 _destination;
    [SerializeField]private int _maxHp = 100;
    [SerializeField]private List<Vector3> _destinationPoints = new List<Vector3>();
    [SerializeField] internal int _points = 0;
    [SerializeField]private int _destinationIndex = 0;
    [SerializeField] internal float _speed  = 3;
    [SerializeField] private bool _isMoving = true;
    private void Start()
    {
        _destination = transform.position + _destinationPoints[0];
        _hp = _maxHp;
    }

    protected virtual void Behave()
    {
        if (_isMoving)
        {
            Traverse();
        }
    }

    private int NextDestination(List<Vector3> destinationList,int currDest)
    {
        if (currDest<destinationList.Count-1)
        {
            return currDest+1;
        }
        else 
        {
            return 0;
        }
    }
    protected void Traverse()
    {
        if (Vector2.Distance(transform.position, _destination) > .5f)
        {
            transform.position += new Vector3(_destination.x - transform.position.x,_destination.y - transform.position.y,0).normalized * (_speed * Time.deltaTime);
        }
        else
        {
            _destinationIndex = NextDestination(_destinationPoints, _destinationIndex);
            _destination = transform.position + _destinationPoints[_destinationIndex];
        }
    }

    protected virtual void ShootPlayer()
    {
        
    }

    public void OnDamage(int damage)
    {
        if (_hp - damage <= 0)
        {
            OnDeath();
            _hp = _maxHp;
        }
        else
        {
            _hp -= damage;
        }
    }
    protected virtual void OnDeath()
    {
        GameController.instance.AddPoints(_points);
        gameObject.SetActive(false);
    }

    private void OnBecameInvisible()
    {
        Destroy(this.gameObject);
    }
    private void OnDrawGizmosSelected()
    {
        for (var i = 0; i < _destinationPoints.Count; i++)
        {
            Gizmos.DrawSphere(transform.position +_destinationPoints[i],.1f);
        }
    }
}
