﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HighScoreTable : MonoBehaviour
{
    [SerializeField]private TMP_InputField nameField;
    private Transform entryContainer;
    private Transform entryTemplate;
    private List<HighscoreEntry> highscoreEntryList;
    private List<Transform> highscoreEntryTransformList;

    private void Awake()
    {
        DisplayHighscore();
        //DeleteAllHighScore();
    }

    public void DisplayHighscore()
    {
        entryContainer = transform.Find("HighScoreEntryContainer");
        for (int i = 0; i < entryContainer.childCount; i++)
        {
            if (entryContainer.GetChild(i).name != "HighScoreEntryTemplate")
            {
                Destroy(entryContainer.GetChild(i).gameObject);
            }
        }
        entryTemplate = entryContainer.Find("HighScoreEntryTemplate");
        
        entryTemplate.gameObject.SetActive(false);

        string jsonString = PlayerPrefs.GetString("highscoreTable");
        HighScores highscores = JsonUtility.FromJson<HighScores>(jsonString);
        
        for (int i = 0; i < highscores.highscoreEntryList.Count; i++)
        {
            for (int j = 0; j < highscores.highscoreEntryList.Count; j++)
            {
                if (highscores.highscoreEntryList[j].score < highscores.highscoreEntryList[i].score)
                {
                    HighscoreEntry tmp = highscores.highscoreEntryList[i];
                    highscores.highscoreEntryList[i] = highscores.highscoreEntryList[j];
                    highscores.highscoreEntryList[j] = tmp;
                }
            }
        }
        highscoreEntryTransformList = new List<Transform>();
        if (highscores.highscoreEntryList.Count < 10)
        {
            for (int i = 0; i < highscores.highscoreEntryList.Count; i++)
            {
                CreateHighScoreEntryTransform(highscores.highscoreEntryList[i],entryContainer,highscoreEntryTransformList);
            }   
        }
        else
        {
            for (int i = 0; i < 10; i++)
            {
                CreateHighScoreEntryTransform(highscores.highscoreEntryList[i],entryContainer,highscoreEntryTransformList);
            }    
        }
    }

    private void DeleteAllHighScore()
    {
        string jsonString = PlayerPrefs.GetString("highscoreTable");
        HighScores highscores = JsonUtility.FromJson<HighScores>(jsonString);
        highscores = new HighScores();
        string json = JsonUtility.ToJson(highscores);
        PlayerPrefs.SetString("highscoreTable",json);
        PlayerPrefs.Save();
    }
    private void CreateHighScoreEntryTransform(HighscoreEntry highscoreEntry, Transform container,
        List<Transform> transformList)
    {
        float templateHeight = 30f;
        Transform entryTransform = Instantiate(entryTemplate, container);
            RectTransform entryRectTransform = entryTransform.GetComponent<RectTransform>();
            entryRectTransform.anchoredPosition = new Vector2(0,-templateHeight*transformList.Count);
            entryTransform.gameObject.SetActive(true);

            int rank = transformList.Count + 1;
            string rankString;
            switch (rank)
            {
                default:
                    rankString = rank + "TH";
                    break;
                case 1:
                    rankString = "1ST";
                    break;
                case 2:
                    rankString = "2ND";
                    break;
                case 3:
                    rankString = "3RD";
                    break;
            }
            entryTransform.Find("posText").GetComponent<TextMeshProUGUI>().text = rankString;

            int score = highscoreEntry.score;
            entryTransform.Find("scoreText").GetComponent<TextMeshProUGUI>().text = score.ToString();

            string name = highscoreEntry.name;
            entryTransform.Find("nameText").GetComponent<TextMeshProUGUI>().text = name;
            
            entryTransform.Find("background").gameObject.SetActive(rank%2==1);
            
            transformList.Add(entryTransform);
    }
    
    public void AddHighscoreEntry()
    {
        HighscoreEntry highscoreEntry = new HighscoreEntry{score = GameController.instance.GetScore(), name = nameField.text};
        
        string jsonString = PlayerPrefs.GetString("highscoreTable");
        HighScores highscores = JsonUtility.FromJson<HighScores>(jsonString);

        if (highscores == null)
        {
            highscores = new HighScores();
            highscores.highscoreEntryList.Add(highscoreEntry);
        }
        else
        {
            highscores.highscoreEntryList.Add(highscoreEntry);
        }
        
        string json = JsonUtility.ToJson(highscores);
        PlayerPrefs.SetString("highscoreTable",json);
        PlayerPrefs.Save();
    }
    
    private void AddHighscoreEntry(int score, string name)
    {
        HighscoreEntry highscoreEntry = new HighscoreEntry{score = score, name = name};
        
        string jsonString = PlayerPrefs.GetString("highscoreTable");
        HighScores highscores = JsonUtility.FromJson<HighScores>(jsonString);
        
        highscores.highscoreEntryList.Add(highscoreEntry);
        
        string json = JsonUtility.ToJson(highscores);
        PlayerPrefs.SetString("highscoreTable",json);
        PlayerPrefs.Save();
    }
    private class HighScores
    {
        public List<HighscoreEntry> highscoreEntryList;
    }
[System.Serializable]
    private class HighscoreEntry
    {
        public int score;
        public string name;
    }
    
}
