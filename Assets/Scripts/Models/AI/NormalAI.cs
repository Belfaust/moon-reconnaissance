﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class NormalAI : AIBase
{
    [SerializeField]private float reloadTime = 1;
    private void Awake()
    {
        ShootPlayer();
    }

    private void Update()
    {
        Behave();
    }

    protected override void Behave()
    {
        base.Behave();   
    }
    protected override void OnDeath()
    {
        base.OnDeath();
    }
    
    protected override void ShootPlayer()
    {
        base.ShootPlayer();
        StartCoroutine(Reload());
    }

    IEnumerator Reload()
    {
        while(true)
        {
            yield return new WaitForSeconds(reloadTime);
            GameObject Bullet = Instantiate(bullet, transform.position, Quaternion.identity,transform.parent);
            Bullet.GetComponent<Rigidbody>().velocity = new Vector3(0,Random.Range(-.1f,.1f),0);
            Bullet.transform.rotation = Quaternion.Euler(0, 0, -180);
        }
    }
}
