﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    private float _length, _startpos;
    [SerializeField] private GameObject _myCam;
    public float parallaxEffect;
    private void Start()
    {
        _startpos = transform.position.x;
        _length = GetComponent<SpriteRenderer>().bounds.size.x;
       

    }

    private void Update()
    {
        float temp = (_myCam.transform.position.x * (1 - parallaxEffect));
        float dist = (_myCam.transform.position.x * parallaxEffect);
        var offset = new Vector3(_startpos + _length,0,0); 
        transform.Translate(-2.5f *Time.deltaTime,0,0);
        //transform.position = new Vector3(_startpos+ dist,transform.position.y,0);
        if (transform.position.x > _startpos + _length)
            transform.position -= offset;
        else if (transform.position.x < _startpos - _length)
        {
            transform.position += offset;
        }
    }
}
