﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;
using DG.Tweening;


public class SectorCutscene : MonoBehaviour
{
    [SerializeField]private TextMeshProUGUI _sectorTitle, _playerTime, _sectorAvgTime, _topRecord, _bonusPoints,_brokenRecord;
    [SerializeField]private GameObject _scoreScreen;
    private const string _sectorTitleText = "Time To Reach Point",_playerTimeText = "Your Time :",_sectorAvgTimeText = "The Average time :";
    private void Start()
    {
        DOTween.Init();
    }

    public IEnumerator MainSectorStop(int levelProgress,int time,Slider progressBar)
    {
        while (true)
        {
            GameController.instance.StopGame();
            _brokenRecord.text = "";
            _scoreScreen.SetActive(true);
            var pointsToAdd = 5000;
            int lastTime = PlayerPrefs.GetInt(Convert.ToChar((int) (progressBar.value / 20) + 64).ToString());
            _sectorTitle.text = $"{_sectorTitleText} '{Convert.ToChar((int)(progressBar.value / 20)+64).ToString()}'";
            yield return new WaitForSeconds(.5f);
            _playerTime.text = $"{_playerTimeText} {time}";
            yield return new WaitForSeconds(.5f);
            _sectorAvgTime.text = $"{_sectorAvgTimeText} {lastTime}";
            yield return new WaitForSeconds(.5f);
            if (time < lastTime)
            {
                _topRecord.text = $"Top Record : {time}";
                for (int i = lastTime; i > time-1; i--)
                {
                    _sectorAvgTime.text = $"{_sectorAvgTimeText} {(lastTime-((pointsToAdd-5000)/100))}";
                    _bonusPoints.text = $"Good Bonus Points : {pointsToAdd}";
                    pointsToAdd += 100;
                    yield return new WaitForSeconds(.2f);
                }
                _brokenRecord.text = "You have Broken a record!";
            }
            else
            {
                _topRecord.text = $"Top Record :{PlayerPrefs.GetInt(Convert.ToChar((int) (progressBar.value / 20) + 64).ToString())}";
                _bonusPoints.text = $"Good Bonus Points: {pointsToAdd}";
            }
            GameController.instance.AddPoints(pointsToAdd);
            yield return new WaitForSeconds(5f);
            _scoreScreen.SetActive(false);
            _sectorTitle.text = _sectorTitleText;
            _playerTime.text = _playerTimeText;
            _sectorAvgTime.text = _sectorAvgTimeText;
            GameController.instance.ResumeGame();
            break;
        }
    }
}
