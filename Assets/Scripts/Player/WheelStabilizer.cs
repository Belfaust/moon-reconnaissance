﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelStabilizer : MonoBehaviour
{
    [SerializeField] private ParticleSystem myParticles;
    private Vector3 _localPos;
    
    void Start()
    {
        _localPos = transform.localPosition;
    }
    void Update()
    {
        var currentLocalPos = transform.localPosition;
        transform.localPosition = new Vector3(_localPos.x,transform.localPosition.y ,transform.localPosition.z);
        if (currentLocalPos.y > _localPos.y+.2f)
        {
            transform.localPosition = new Vector3(currentLocalPos.x,_localPos.y+.2f,currentLocalPos.z);
        }
    }

    private void OnCollisionStay(Collision other)
    {
        if (!other.gameObject.CompareTag("Player"))
        {
            for (int i = 0; i < other.contacts.Length; i++)
            {
                if (!other.contacts[i].otherCollider.gameObject.CompareTag("Player"))
                {
                    myParticles.gameObject.transform.position = other.contacts[i].point;
                    break;
                }
            }
            if (!myParticles.isPlaying)
            {
                myParticles.Play();
            }
        }
    }

    private void OnCollisionExit(Collision other)
    {
        if (!other.gameObject.CompareTag("Player"))
        {
            myParticles.Stop();
        }
    }
}
