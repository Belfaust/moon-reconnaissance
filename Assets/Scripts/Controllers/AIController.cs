﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using UnityEngine;

public class AIController : MonoBehaviour
{
    [SerializeField] private GameObject Boulder;
    [SerializeField] private EnemyWave[] aiWaveList;
    private bool isSpawning;
    
    public void ExecuteRandomWave()
    {
        var randomNumber = Random.Range(0, aiWaveList.Length);
        if(!isSpawning)
            StartCoroutine(StartSpawning(randomNumber));
    }
    public void CleanEnemies()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }
    }

    public void StopExecuting()
    {
        StopAllCoroutines();
    }
    IEnumerator StartSpawning(int waveIndex)
    {
        var aiIndex = 0;
        isSpawning = true;
        while (aiIndex < aiWaveList[waveIndex].Enemies.Count)
        {
            var aiBase = aiWaveList[waveIndex].Enemies[aiIndex].GetComponent<AIBase>();
            Instantiate(aiWaveList[waveIndex].Enemies[aiIndex],
                new Vector3(
                    Random.Range(aiBase.leftSpawnCorner.x, aiBase.rightSpawnCorner.x),
                    Random.Range(aiBase.leftSpawnCorner.y, aiBase.rightSpawnCorner.y),
                    0)
                ,Quaternion.identity,transform);
            yield return new WaitForSeconds(aiWaveList[waveIndex].enemySpawnRate);
            aiIndex++;
        }

        isSpawning = false;
    }

    public IEnumerator BoulderWave(int boulderAmount)
    {
        var currentBoulders = 0;
        while (currentBoulders <boulderAmount)
        {
            Instantiate(Boulder, new Vector3(14, 2, 0),Quaternion.identity,transform);
            currentBoulders++;
            yield return new WaitForSeconds(2.5f);
        }
        
    }
}
[System.Serializable]
public class EnemyWave
{
    public List<GameObject> Enemies;
    public float enemySpawnRate;
}