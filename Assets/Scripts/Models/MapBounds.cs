﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapBounds : MonoBehaviour
{
   private void OnTriggerStay(Collider other)
   {
      if (other.gameObject.CompareTag("Player"))
      {
         var rb = other.gameObject.GetComponent<Rigidbody>(); 
         rb.velocity = new Vector3(0,rb.velocity.y,0);
         if (other.transform.position.x < 0)
         {
            rb.AddForce(new Vector3(5,0,0));
         }
         else
         {
            rb.AddForce(new Vector3(-5,0,0));
         }
      }
   }
}
