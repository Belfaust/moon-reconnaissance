﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class TerrainShader : MonoBehaviour
{
    public Material myShader;
    private float _Hue = 0, _Pixel = 10;
    float maxHue = 360;
    float maxPixel = 25;

    private void Start()
    {
        DOTween.Init();
        StartCoroutine(ShaderAnim());
    }
    private void Update()
    {
        myShader.SetFloat("Hue",_Hue);
        myShader.SetFloat("Pixel",_Pixel);
    }
    private IEnumerator ShaderAnim()
    {
        while (true)
        {
            DOTween.To(() => _Hue, x => _Hue = x, maxHue, 10);
            DOTween.To(() => _Pixel, x => _Pixel = x, maxPixel, 10);
            yield return new WaitUntil(() => _Hue == maxHue);
            yield return new WaitForSeconds(.1f);
            _Pixel = 10;
            _Hue = 0;
        }
    }
}
