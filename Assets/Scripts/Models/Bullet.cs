﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private int _damage;
    [SerializeField] private bool _isPooled;
    [SerializeField]private float _speed = 10;
    [SerializeField] private GameObject _onHitParticle;
    private bool _isMoving;
    private void Update()
    {
        if(_isMoving)
            transform.Translate(Vector3.up*_speed*Time.deltaTime);
    }

    private void OnBecameInvisible()
    {
        _isMoving = false;
        if (_isPooled)
        {
            gameObject.SetActive(false);
        }
        else
        { 
            Destroy(gameObject);    
        }
    }

    private void OnBecameVisible()
    {
        _isMoving = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out AIBase enemy)&&!transform.CompareTag("Enemy"))
        {
            enemy.OnDamage(_damage);
            if (_isPooled)
            {
                Instantiate(_onHitParticle, transform.position, Quaternion.identity);
                FMODUnity.RuntimeManager.PlayOneShot("event:/Player",GetComponent<Transform>().position);
                gameObject.SetActive(false);
            }
        }
        else if(transform.CompareTag("Enemy")&&!other.CompareTag("Enemy"))
        {
            Instantiate(_onHitParticle, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
