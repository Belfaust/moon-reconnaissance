﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticlesCleaner : MonoBehaviour
{
    private ParticleSystem _thisParticle;
    void Start()
    {
        _thisParticle = GetComponent<ParticleSystem>();
    }
    void Update()
    {
        if (_thisParticle.isPlaying)
            return;
        
        Destroy(gameObject);
    }
}
