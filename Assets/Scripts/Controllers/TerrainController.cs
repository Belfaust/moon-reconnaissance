﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainController
{
    public List<GameObject> terrainPool = new List<GameObject>();
    private int segmentIndex,segmentHeightScale =5;
    private int segmentLength = 6,segmentHeight = 6,poolingLength = 10;
    private float randomSeed;

    public void SetAllSpeed(float speed)
    {
        for (int i = 0; i < terrainPool.Count; i++)
        {
            terrainPool[i].GetComponent<TerrainMovement>().Speed = speed;
        }
    }
    private float OctavePerlin(float x,float scale, int octaves, float persistence,float lacunarity,float seed) {
        float total = 0;
        float frequency = 1;
        float amplitude = 1;
        float maxValue = 0;
        for(int i=0;i<octaves;i++)
        {
            var samplex = x / scale * frequency+seed;
            total += Mathf.PerlinNoise(samplex, 0) * amplitude;
            maxValue += amplitude;
        
            amplitude *= persistence;
            frequency *= lacunarity;
        }
        return total/maxValue;
    }
    
    public void CreateStartingMeshPool(MeshRenderer mapMeshRender,GameObject chunkPool)
    {
        while (segmentIndex < poolingLength)
        {
            GameObject newChunk = new GameObject("Chunk " + segmentIndex,typeof(MeshFilter),typeof(MeshRenderer),typeof(TerrainMovement));
            newChunk.GetComponent<TerrainMovement>().segmentMeshLength = segmentLength;
            newChunk.GetComponent<TerrainMovement>().terrainController = this;
            newChunk.transform.position = new Vector3(10+(segmentIndex*(segmentLength-1)),-10,0);
            MeshData meshData = GenerateMeshSegment();
            newChunk.GetComponent<MeshFilter>().sharedMesh = meshData.CreateMesh();
            newChunk.GetComponent<MeshRenderer>().sharedMaterials = mapMeshRender.sharedMaterials;
            newChunk.AddComponent<MeshCollider>();
            newChunk.transform.SetParent(chunkPool.transform);
            terrainPool.Add(newChunk);
            GameController.instance.ProgressLevel(-1);
        }
       
    }
    public void ApplyNewMesh(GameObject gameObject)
    {
        MeshData meshData;
        int currentIndex = terrainPool.IndexOf(gameObject);
        if (GameController.instance.setPit)
        {
            if (currentIndex + 2 < terrainPool.Count)
            {
                terrainPool[currentIndex+2].GetComponent<TerrainMovement>().isaPit = true;
            }
            else
            {
                terrainPool[(currentIndex + 2) - terrainPool.Count].GetComponent<TerrainMovement>().isaPit = true;
            }
            GameController.instance.setPit = false;
        }
        if(terrainPool[currentIndex].GetComponent<TerrainMovement>().isaPit)
        {
            meshData = GeneratePit();
            terrainPool[currentIndex].GetComponent<TerrainMovement>().isaPit = false;
        }
        else
        {
            meshData = GenerateMeshSegment();
        }
        gameObject.GetComponent<MeshFilter>().sharedMesh = meshData.CreateMesh();
    }
    public struct MeshData{
        public List <Vector3> vertices;
        public List<int> triangles;
        public List<Vector2> uvs;
        int _triangleIndex;

        public MeshData(int triangleIndex)
        {
            vertices = new List<Vector3>();
            triangles = new List<int>();
            uvs = new List<Vector2>();
            _triangleIndex = triangleIndex;
        }

        public void AddTriangle(int a,int b,int c)
        {
            triangles.Add(0);
            triangles.Add(0);
            triangles.Add(0);
            triangles [_triangleIndex] = a;
            triangles [_triangleIndex+1] = b;
            triangles [_triangleIndex+2] = c;
            _triangleIndex +=3;

        }
        public Mesh CreateMesh(){
            Mesh mesh = new Mesh();
            mesh.vertices = vertices.ToArray();
            mesh.triangles = triangles.ToArray();
            mesh.uv = uvs.ToArray();
            mesh.RecalculateNormals();
            return mesh;
        }

    }
    public MeshData CreateStartingLevel(int width,int height,Vector3 startingPosition)
    {
        MeshData meshData = new MeshData(0);
        int vertexIndex = 0;
        randomSeed = Random.Range(-10000, 10000);
        for (int y = 0;y <= height ; y++)
        {
            for (int x = 0; x <= width ; x++)
            {
                meshData.vertices.Add(new Vector3(x*startingPosition.x,y * startingPosition.y,0)); 
                if (x < width && y < height)
                {
                    //Generating the side Wall
                    meshData.AddTriangle(vertexIndex,vertexIndex + width+1,vertexIndex + 1 );
                    meshData.AddTriangle(vertexIndex + 1,vertexIndex + width+1,vertexIndex + width + 2  );
                }
                else if (y==height&&x==width)
                {
                    //Adding the top Floor 
                    meshData.vertices.Add(new Vector3((x-1)*startingPosition.x,width* startingPosition.y,1));
                    meshData.vertices.Add(new Vector3(x*startingPosition.x,y* startingPosition.y,1));
                    meshData.AddTriangle(vertexIndex-1,vertexIndex + width, vertexIndex );
                    meshData.AddTriangle(vertexIndex,vertexIndex + width ,vertexIndex + width + 1 );
                }
                vertexIndex++;
            }
        }
        return meshData;
    }

    private MeshData GeneratePit()
    {
         MeshData meshData = new MeshData(0);
         float pitScale = .8f;
            for (int x = 0; x < segmentLength; x++)
            {
                float height = OctavePerlin(segmentIndex * (segmentLength - 1) + x, 4, 8, 1.3f, 2f, randomSeed);
                for (int y = 0; y < 3; y++)
                {
                    if (y != 2)
                    {
                        if (x == 0 || x == segmentLength - 1)
                        {
                            meshData.vertices.Add(new Vector3(x, y * 20 * height, 0));
                        }
                        else
                        {
                            meshData.vertices.Add(new Vector3(x, y * 20 * height*pitScale, 0));
                        }
                    }
                    else
                    {
                        if (x == 0 || x == segmentLength - 1)
                        {
                            meshData.vertices.Add(new Vector3(x, (y - 1) * 20 * height, 1));
                        }
                        else
                        {
                            meshData.vertices.Add(new Vector3(x, (y - 1) * 20 * height*pitScale, 1));
                        }
                    }
                }
            }
            var vertexIndex = 0;
            for (int x = 0; x < segmentLength; x++)
            {
                for (int y = 0; y < 3; y++)
                {
                    if (x < segmentLength - 1 && y < 2)
                    {
                        meshData.AddTriangle(vertexIndex, vertexIndex + 1, vertexIndex + 3);
                        meshData.AddTriangle(vertexIndex + 3, vertexIndex + 1, vertexIndex + 4);
                    }

                    vertexIndex++;
                }
            }
            segmentIndex++;
            GameController.instance.ProgressLevel(1);
            return meshData;
    }
    private MeshData GenerateMeshSegment()
    {
        MeshData meshData = new MeshData(0);
            for (int x = 0; x < segmentLength; x++)
            {
                float height = OctavePerlin(segmentIndex * (segmentLength - 1) + x, 4, 8, 1.3f, 2f, randomSeed)*segmentHeightScale;
                for (int y = 0; y < segmentHeight; y++)
                {
                    //Checking for starting pos of the first segment so that it properly connects
                    if (segmentIndex == 0 && x == 0)
                    {
                        if (y < segmentHeight - 2)
                        {
                            meshData.vertices.Add(new Vector3(x, y*height, 0));
                        }
                        else
                        {
                            meshData.vertices.Add(new Vector3(x, 10, y-segmentHeight+2));
                        }
                    }
                    //Then proceeding to generate the terrain normally 
                    else if (y < segmentHeight-1)
                    {
                        meshData.vertices.Add(new Vector3(x, y *height, 0));
                    }
                    else
                    {
                        meshData.vertices.Add(new Vector3(x, (y - 1) *height, 1));
                    }
                    meshData.uvs.Add(new Vector2(x,y));
                }
            }
            var vertexIndex = 0;
            for (int x = 0; x < segmentLength; x++)
            {
                for (int y = 0; y < segmentHeight; y++)
                {
                    if (x < segmentLength - 1 && y < segmentHeight-1)
                    {
                        //The vertex are made this way 
                        // 2 5 8
                        // 1 4 7
                        // 0 3 6
                        //That;s why the faces are connecting first the 0 1 3 then 3 1 4
                        //this applies to every face 
                        //that's also why the faces are not made at the edges
                        meshData.AddTriangle(vertexIndex, vertexIndex + 1, vertexIndex + segmentHeight);
                        meshData.AddTriangle(vertexIndex + segmentHeight, vertexIndex + 1, vertexIndex + segmentHeight+1);
                    }
                    vertexIndex++;
                }
            }
        
            segmentIndex++;
            GameController.instance.ProgressLevel(1);
            return meshData;
        
    }
}
